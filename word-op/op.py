#!/usr/bin/python
#-*- coding: utf-8 -*-
#Author:liujian
#Date:2014/07/28 15:40:04
#Dec:词操作库

import sys
import re


#判断一个词是否是中文词
def is_ch(term):
    return ptn_ch_whole.match(term.decode('utf-8')) != None


#判断是否包含中文词
def have_ch(term):
    return ptn_ch.search(term.decode('utf-8')) != None

#中文词pattern
ptn_ch = re.compile(ur"[\u4e00-\u9fa5]+")
ptn_ch_whole = re.compile(ur"^[\u4e00-\u9fa5]+$")
#

