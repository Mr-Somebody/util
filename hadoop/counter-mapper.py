#!/usr/bin/python
#-*- coding: utf-8 -*-
#Author:liujian
#Date:2014/07/28 11:42:10
#Dec:hadoop计数器mapper

import sys
import re


# 解析输入
def parse_key_index(istr):
    matcher = pt_multi.match(istr)
    if matcher:
        return (1, [int(v) for v in matcher.group().split(',')])
    matcher = pt_range.match(istr)
    if matcher:
        start, end = matcher.group().split('-')
        if len(end) == 0:
            end = sys.maxint
        return (0, [int(start), int(end)])
    return (-1, None)

# 键值格式
pt_range = re.compile(r'^\d+(\-(\d+)?)?$')
pt_multi = re.compile(r'^\d+(,\d+)*$')
# 计数键值位置，格式为\d+[[\-,]\d+]*。例如：0-3代表索引值[0,3);0,3代表0和3
KEY_INDEX = '1-'
# 切割分割符
SEPARATOR=None
def main():
    ki_type, kis = parse_key_index(KEY_INDEX)
    if ki_type == -1:
        return
    for line in sys.stdin:
        fields = line.strip().split(SEPARATOR)
        if ki_type == 0:
            start = max(0, min(kis[0], len(fields)))
            end = max(0, min(kis[1], len(fields)))
            for i in range(start, end):
                print fields[i]
        elif ki_type == 1:
            for i in kis:
                if i < len(fields):
                    print fields[i]
# 
if __name__ == '__main__':
    main()
    
