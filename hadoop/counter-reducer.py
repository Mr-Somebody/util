#!/usr/bin/python
#-*- coding: utf-8 -*-
#Author:liujian
#Date:2014/07/28 15:21:45
#Dec:hadoop计数器reducer

import sys

cur_value = ''
counter = 0
for line in sys.stdin:
    line = line.strip()
    if cur_value != line:
        print '%s\t%d' % (cur_value, counter)
        cur_value = line
        counter = 0
    counter += 1
print '%s\t%d' % (cur_value, counter)
